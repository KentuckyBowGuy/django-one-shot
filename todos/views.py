from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.\


def todo_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list": todos}
    return render(request, "todos/todo_list.html", context)


def todo_detail(request, id):
    todo = TodoList.objects.get(id=id)
    context = {"todo": todo}
    return render(request, "todos/todo_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("todo_list_detail", form.instance.id)
    # why does return work here?
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_list.html", context)


def update_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            test1 = form.save()
            return redirect("todo_list_detail", test1.id)
    else:
        form = TodoForm(instance=todo_list)
    context = {
        "form": form,
    }
    return render(request, "todos/update_list.html", context)


def delete_list(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete_list.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", form.instance.list.id)
            # why does return work here?
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/item_create.html", context)


def update_item(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            test2 = form.save()
            return redirect("todo_list_detail", test2.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "form": form,
    }
    return render(request, "todos/item_update.html", context)
